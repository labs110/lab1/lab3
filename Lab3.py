import math
import matplotlib.pyplot as plt

# Блок ввода данных
while True:
    x_array, y_array = [], []
    a, x, x_lower, x_upper, value, choice = 0, 0, 0, 0, 0, 0
    try:
        a = float(input('Введите  переменную а: '))
        x_lower, x_upper = float(input('Ввод нижней границы x: ')), float(input('Ввод верхней границы x: '))
        value = int(input('Выберите значение, где G == 1, F == 2, Y == 3 : '))
        choice = int(input('[1 -   Ввод количества шагов || 2 - Длина шага]: '))
    except ValueError:
        print('Ошибка при вводе данных')
        exit(1)

    try:
        if choice == 1:
            H = int(input('Количество шагов  '))
            x_step = (x_upper - x_lower) / H
        elif choice == 2:
            x_step = float(input('Введите значение шага: '))
    except ValueError:
        print('Ошибка ввода данных')
        exit(2)

    # Блок расчёта и вывода данных G
    if value == 1:
        x = x_lower
        while x < x_upper:
            try:
                G = (6 * (4 * a ** 2 - 12 * a * x + 5 * x ** 2)) / (9 * a ** 2 + 30 * a * x + 16 * x ** 2)
                print('x = {:5f}  G = {:5f}'.format(x, G))
                x_array.append(x)
                y_array.append(G)
                plt.ylabel('G(x)')
            except ZeroDivisionError:
                print(f'Ошибка расчёта значения G при х = {x}')
                x_array.append(x)
                y_array.append(None)

            x += x_step

    # Блок расчёта и вывода данных F
    elif value == 2:
        x = x_lower
        while x < x_upper:
            F = 5 ** (a ** 2 - 5 * a * x + 4 * x ** 2)
            print('x = {:5f}  G = {:5f}'.format(x, F))
            x_array.append(x)
            y_array.append(F)
            plt.ylabel('F(x)')

            x += x_step

    # Блок расчёта и вывода данных Y
    elif value == 3:
        x = x_lower
        while x < x_upper:
            try:
                Y = math.log(-a ** 2 - 7 * a * x + 8 * x ** 2 + 1) / math.log(2)
                print('x = {:5f}  G = {:5f}'.format(x, Y))
                x_array.append(x)
                y_array.append(Y)
                plt.ylabel('Y(x)')
            except ValueError:
                print(f'Ошибка расчёта значения Y при х = {x}')
                x_array.append(x)
                y_array.append(None)

            x += x_step
        # Блок ошибки ввода значения value
    else:
        print('Такого значения не существует')

    # Вывод данных на график
    plt.title('График функции')
    plt.plot(x_array, y_array)
    plt.xlabel('x')
    plt.grid()
    plt.show()

    # Завершение/продолжение программы
    while True:
        try:
            ext = int(input("Начать сначала? 1 - Да | 2 - Нет "))
            break
        except ValueError:
            print('Ввод не распознан. Повторите еще раз.')
        continue

    if ext == 2:
        print('Вычисления окончены. ')
        break